<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::view("/test","test");

Route::get("/form","register_controler@form");

Route::get("/welcome","register_controler@welcome");
Route::post("/welcome","register_controler@welcome_post");

Route::get('/master', function(){
    return view('master');
});

Route::get('/master/table', function(){
    return view('master.table');
});

